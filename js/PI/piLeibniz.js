let mouseState;
let pi = 4;
let ite = 0;
let history = [];
let den;

let minY = 2;
let maxY = 4;

function setup() {
    let divHeight = document.getElementById("pi").clientHeight;
    let divWidth = document.getElementById("pi").clientWidth;

    let canvas = createCanvas(divWidth, divHeight - 100);
    canvas.parent('pi');

    center = createVector(width / 2, height / 2);

    frameRate(5);

    mouseState = false;
    background(255);

}

function draw() {
    clear();
    den = ite * 2 + 3;
    let frac = 4 / den;
    if (ite % 2 === 0)
        pi -= frac;
    else
        pi += frac;

    history.push(pi);

    let piy = map(PI, minY, maxY, height, 0);
    stroke(0);
    line(0,  piy, width, piy);

    let spacing = width/history.length;
    noFill();
    stroke(220, 75, 0);
    beginShape();
    for (let i = 0; i < history.length; i++) {
        let x = i * spacing;
        let y = map(history[i], minY, maxY, height, 0);

        vertex(x, y);
    }
    endShape();

    ite++;

    let value = pi;
    document.getElementById("pi_value").innerHTML = value.toFixed(5).toString();
    document.getElementById("vertex").innerHTML = history.length.toString();

}

function play_pause() {
    if (!mouseState) {
        noLoop();
        mouseState = true;
        document.getElementById('btn_pp').className = "fa fa-play";
    } else {
        loop();
        mouseState = false;
        document.getElementById('btn_pp').className = "fa fa-pause";

    }
}