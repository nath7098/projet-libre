let radius = 175;
let center;
let nbPoints = 3;
let mouseState = false;

function setup() {
    let divHeight = document.getElementById("pi").clientHeight;
    let divWidth = document.getElementById("pi").clientWidth;

    let canvas = createCanvas(divWidth, divHeight - 100);
    canvas.parent('pi');

    center = createVector(width / 2, height / 2);

    mouseState = false;

    frameRate(2);

    background(255);
}

function draw() {
    clear();
    noFill();
    stroke(200,0,0);
    strokeWeight(1);
    ellipse(center.x, center.y, radius * 2);

    stroke(100, 100, 0);
    strokeWeight(2);
    let value = polygon(center.x, center.y, radius, nbPoints);
    nbPoints++;
    value /= radius * 2.0;

    document.getElementById("pi_value").innerHTML = value.toFixed(5).toString();
    document.getElementById("vertex").innerHTML = nbPoints.toString();

}

function polygon(x, y, radius, npoints) {
    let angle = TWO_PI / npoints;
    beginShape();
    let previous = createVector(x, y);
    let current;
    let d = 0;
    for (let a = 0; a < TWO_PI; a += angle) {
        let sx = x + cos(a) * radius;
        let sy = y + sin(a) * radius;
        vertex(sx, sy);
        d += dist(previous.x, previous.y, sx, sy);
        previous = createVector(sx, sy);
    }
    endShape(CLOSE);
    return d;
}

function play_pause() {
    if (!mouseState) {
        noLoop();
        mouseState = true;
        document.getElementById('btn_pp').className = "fa fa-play";
    } else {
        loop();
        mouseState = false;
        document.getElementById('btn_pp').className = "fa fa-pause";

    }
}