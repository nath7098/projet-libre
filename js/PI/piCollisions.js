let mouseState;
let block1;
let block2;
let blockImg;
let digits;
let collisions;
const timesteps = 1000000;
var inputDigits = $("#digits");

function preload() {
    blockImg = loadImage('../../img/pika.jpg');
}

inputDigits.on("change", function (event) {
    setup();
    digits = int(inputDigits.val()) + 1;
});

function setup() {

    let divHeight = document.getElementById("pi").clientHeight;
    let divWidth = document.getElementById("pi").clientWidth;

    let canvas = createCanvas(divWidth, divHeight - 100);
    canvas.parent('pi');

    mouseState = false;

    collisions = 0;

    digits = int(inputDigits.val()) + 1;

    block1 = new Block(100, 50, 0, 1);
    block2 = new Block(200, (digits) * 50, -3 / timesteps, Math.pow(100, digits - 1));

}

function draw() {
    background(255);

    for (let i = 0; i < timesteps; i++) {
        if (block1.collide(block2)) {
            const v1 = block1.bounce(block2);
            const v2 = block2.bounce(block1);
            block1.v = v1;
            block2.v = v2;
            collisions++;
        }

        if (block1.hitWall()) {
            block1.reverse();
            collisions++;
        }
        block2.hitWall();

        block1.update();
        block2.update();
    }

    block1.show();
    block2.show();

    $("#pi_value").text(collisions.toString());

}

function play_pause() {
    if (!mouseState) {
        noLoop();
        mouseState = true;
        document.getElementById('btn_pp').className = "fa fa-play";
    } else {
        loop();
        mouseState = false;
        document.getElementById('btn_pp').className = "fa fa-pause";

    }
}