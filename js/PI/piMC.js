let radius = 125;
let dots = [];
let nbDotsPerFrame = 1000;
let center;
let mouseState = false;

function setup() {
    let divHeight = document.getElementById("pi").clientHeight;
    let divWidth = document.getElementById("pi").clientWidth;

    let canvas = createCanvas(divWidth, divHeight - 100);
    canvas.parent('pi');

    center = createVector(width / 2, height / 2);

    background(255);
}

function draw() {
    frameRate(8);
    noFill();
    stroke(0);
    strokeWeight(2);
    ellipse(width / 2, height / 2, radius * 2);


    stroke(255, 0, 0);
    strokeWeight(2);
    rect(width / 2 - radius, height / 2 - radius, radius * 2, radius * 2);

    if (dots.length < 150000) {
        for (let i = 0; i < nbDotsPerFrame; i++)
            dots.push(createVector(width / 2 - radius + Math.random() * (radius * 2), height / 2 - radius + Math.random() * radius * 2));
    }

    strokeWeight(1);
    let dotIn = 0;
    for (let i = 0; i < dots.length; i++) {
        if (dist(center.x, center.y, dots[i].x, dots[i].y) > radius)
            stroke(0, 255, 0, 100);
        else {
            stroke(0, 0, 255, 100);
            dotIn++;
        }
        point(dots[i].x, dots[i].y);
    }

    let value = dotIn / dots.length * 4;

    document.getElementById("pi_value").innerHTML = "Pi approximation = dots in the circle / total number of dots x 4 = " + value.toFixed(5).toString();
    document.getElementById("nb_points").innerHTML = "number of points = " + dots.length;
}

function play_pause() {
    if (!mouseState) {
        noLoop();
        mouseState = true;
        document.getElementById('btn_pp').className = "fa fa-play";
    } else {
        loop();
        mouseState = false;
        document.getElementById('btn_pp').className = "fa fa-pause";

    }
}


