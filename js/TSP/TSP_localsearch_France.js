let citiesSize = 8;
let recordDistance = 0;
const maxAttempts = 50;
let mouseState;
let bestSol;
let bestI;
let bestJ;
let nnPlayed = false;
let nnBlock = false;
let lcswapsPlayed = false;
let lcswapsBlock = false;
let lc2optPlayed = false;
let lc2optBlock = false;
let elPlayed = false;
let elBlock = false;
let rndPlayed = false;
let rndBlock = false;
let counter = 0;
let solution = 0;
let width;
let height;
let elasticRadius = 5;
let elasticCenter;
let bestFirstText = "";
let bestLcText = "";
let showNames = false;

var sketchLC = function(sketch) {
    let cities = new Cities(sketch);
    let staticCities = new Cities(sketch);
    let bestOrder = new Cities(sketch);
    sketch.preload = function() {
        blockImg = sketch.loadImage('../../img/France.jpg');
    };

    sketch.setup = function() {
        let divHeight = document.getElementById("tsp_localsearch").clientHeight;
        let divWidth = document.getElementById("tsp_localsearch").clientWidth;

        let canvas = sketch.createCanvas(divWidth, divHeight - 100);
        canvas.parent('tsp_localsearch');
        // canvas.style('z-index', '-1');

        // This is the best order !
        width = canvas.width;
        height = canvas.height;

        cities.addCity(new City(width / 3 - 55, height / 3 - 30, citiesSize, sketch, "Brest")); // Brest
        cities.addCity(new City(width / 3 - 30, height / 3 - 10, citiesSize, sketch, "Lorient")); // Lorient
        cities.addCity(new City(width / 3 + 5, height / 3 + 10, citiesSize, sketch, "Nantes")); // Nantes
        cities.addCity(new City(width / 3 + 22, height / 3 + 55, citiesSize, sketch, "La Rochelle")); // La Rochelle
        cities.addCity(new City(width / 3 + 35, height / 3 + 95, citiesSize, sketch, "Bordeaux")); // Bordeaux
        cities.addCity(new City(width / 3 + 40, height / 2 + 90, citiesSize, sketch, "Pau")); // Pau
        cities.addCity(new City(width / 3 + 80, height / 2 + 85, citiesSize, sketch, "Toulouse")); // Toulouse
        cities.addCity(new City(width / 3 + 125, height / 2 + 115, citiesSize, sketch, "Perpignan")); // Perpignan
        cities.addCity(new City(width / 2 - 30, height / 2 + 85, citiesSize, sketch, "Montpellier")); // Montpellier
        cities.addCity(new City(width / 2 - 20, height / 2 + 75, citiesSize, sketch, "Nimes")); // Nimes
        cities.addCity(new City(width / 2 + 10, height / 2 + 92, citiesSize, sketch,"Marseille")); // Marseille
        cities.addCity(new City(width / 2 + 22, height / 2 + 97, citiesSize, sketch,"Toulon")); // Toulon
        cities.addCity(new City(width / 2 + 60, height / 2 + 75, citiesSize, sketch,"Nice")); // Nice
        cities.addCity(new City(width / 2 + 20, height / 3 + 90, citiesSize, sketch,"Grenoble")); // Grenoble
        cities.addCity(new City(width / 2, height / 2 + 5, citiesSize, sketch,"Lyon")); // Lyon
        cities.addCity(new City(width / 2 - 10, height / 2 + 20, citiesSize, sketch,"St Etienne")); // St Etienne
        cities.addCity(new City(width / 3 + 130, height / 2 + 7, citiesSize, sketch, "Clermont-ferrand")); // Clermont-ferrand
        cities.addCity(new City(width / 3 + 80, height / 2, citiesSize, sketch,"Limoges")); // Limoges
        cities.addCity(new City(width / 3 + 60, height / 3 + 42, citiesSize, sketch, "Poitiers")); // Poitiers
        cities.addCity(new City(width / 3 + 40, height / 3 + 5, citiesSize, sketch, "Angers")); // Angers
        cities.addCity(new City(width / 3 + 55, height / 3 - 15, citiesSize, sketch, "Le Mans")); // Le Mans
        cities.addCity(new City(width / 3 + 67, height / 3 + 7, citiesSize, sketch,"Tours")); // Tours
        cities.addCity(new City(width / 3 + 97, height / 3 - 7, citiesSize, sketch,"Orleans")); // Orleans
        cities.addCity(new City(width / 2 - 27, height / 3 - 22, citiesSize, sketch,"Troyes")); // Troyes
        cities.addCity(new City(width / 2 - 2, height / 3 + 10, citiesSize, sketch,"Dijon")); // Dijon
        cities.addCity(new City(width / 2 + 25, height / 3 + 10, citiesSize, sketch,"Besancon")); // Besancon
        cities.addCity(new City(width / 2 + 57, height / 3 - 37, citiesSize, sketch,"Strasbourg")); // Strasbourg
        cities.addCity(new City(width / 2 + 22, height / 3 - 52, citiesSize, sketch,"Metz")); // Metz
        cities.addCity(new City(width / 2 - 25, height / 3 - 55, citiesSize, sketch,"Reims")); // Reims
        cities.addCity(new City(width / 3 + 110, height / 3 - 40, citiesSize, sketch,"Paris")); // Paris
        cities.addCity(new City(width / 3 + 107, height / 3 + -80, citiesSize, sketch,"Amiens")); // Amiens
        cities.addCity(new City(width / 3 + 125, height / 3 - 107, citiesSize, sketch,"Lille")); // Lille
        cities.addCity(new City(width / 3 + 97, height / 3 - 115, citiesSize, sketch,"Calais")); // Calais
        cities.addCity(new City(width / 3 + 57, height / 3 - 65, citiesSize, sketch,"Le Havre")); // Le Havre
        cities.addCity(new City(width / 3 + 40, height / 3 - 55, citiesSize, sketch,"Caen")); // Caen
        cities.addCity(new City(width / 3 + 10, height / 3 - 20, citiesSize, sketch,"Rennes")); // Rennes

        cities.calcDistance();

        staticCities.copy(cities);

        solution = staticCities.myscore;

        cities.list = sketch.shuffle(cities.list);
        cities.calcDistance();

        recordDistance = cities.myscore;
        bestOrder.addCity(cities.list[0]);
        elasticCenter = cities.list[0];
        cities.removeCity(0);
        $("#score_localsearch").text("0.00 %");

    };

    sketch.draw = function() {
        sketch.background(255);

        sketch.frameRate(20);

        sketch.push();
        sketch.translate(width / 4, 0);
        sketch.image(blockImg, 0, 0, height, height);
        sketch.pop();

        staticCities.drawCities(false);

        if (elPlayed) {
            elasticSearch();
            if (!nnPlayed) {
                document.getElementById('launch_el').className = "btn btn-success btn-outline-light ";
            }
            bestFirstText = "elastic search";
        }

        if (nnPlayed) {
            nearestNeighborg();
            if (!nnPlayed) {
                document.getElementById('launch_nn').className = "btn btn-success btn-outline-light ";
            }
            bestFirstText = "nearest neighbor";
        }

        if (rndPlayed) {
            randomShuffle();
            if (!rndPlayed) {
                document.getElementById('launch_rnd').className = "btn btn-success btn-outline-light ";
            }
            bestFirstText = "random";
        }

        if (lcswapsPlayed) {
            cities.copy(bestOrder);
            localSearchSwaps();
            if (!lcswapsPlayed)
                document.getElementById('launch_lcswaps').className = "btn btn-success btn-outline-light ";
            bestLcText = "local search swaps";
        }

        if (lc2optPlayed) {
            cities.copy(bestOrder);
            localSearch2opt();
            if (!lc2optPlayed)
                document.getElementById('launch_lc2opt').className = "btn btn-success btn-outline-light ";
            bestLcText = "local search 2opt";
        }

        bestOrder.drawCities();

        sketch.strokeWeight(1);
        sketch.stroke(0);
        staticCities.drawPaths(true);

        // First city legend

        sketch.strokeWeight(0);
        sketch.fill(255,0,0);
        sketch.ellipse(width - 280, height / 2 - 30, citiesSize * 2);
        sketch.strokeWeight(1);

        sketch.stroke(0);
        sketch.fill(0);
        sketch.textSize(16);
        sketch.textStyle(sketch.NORMAL);
        sketch.text('First City', width - 250, height / 2 - 25);
        sketch.noFill();

        // best path Legend

        sketch.line(width - 300, height / 2 - 5, width - 260, height / 2 - 5);

        sketch.stroke(0);
        sketch.fill(0);
        sketch.textSize(16);
        sketch.textStyle(sketch.NORMAL);
        sketch.text('Best Solution', width - 250, height / 2);
        sketch.noFill();

        // best computed path legend

        if (bestFirstText === "elastic search") {
            sketch.stroke("#55DC7B");
            sketch.line(width - 300, height / 2 + 20, width - 260, height / 2 + 20);

            sketch.fill("#55DC7B");
            sketch.textSize(16);
            sketch.textStyle(sketch.NORMAL);
            sketch.text('Best Path with Elastic Search', width - 250, height / 2 + 25);
            sketch.noFill();
        }

        if (bestFirstText === "nearest neighbor") {
            sketch.stroke("#55DC7B");
            sketch.line(width - 300, height / 2 + 20, width - 260, height / 2 + 20);

            sketch.fill("#55DC7B");
            sketch.textSize(16);
            sketch.textStyle(sketch.NORMAL);
            sketch.text('Best Path with Nearest neighbor', width - 250, height / 2 + 25);
            sketch.noFill();
        }

        if (bestFirstText === "random") {
            sketch.stroke("#55DC7B");
            sketch.line(width - 300, height / 2 + 20, width - 260, height / 2 + 20);

            sketch.fill("#55DC7B");
            sketch.textSize(16);
            sketch.textStyle(sketch.NORMAL);
            sketch.text('Best Path with random', width - 250, height / 2 + 25);
            sketch.noFill();
        }

        if (bestLcText === "local search swaps") {
            sketch.stroke("#55DC7B");
            sketch.fill("#55DC7B");
            sketch.textSize(16);
            sketch.textStyle(sketch.NORMAL);
            sketch.text('And Local Search with swaps', width - 250, height / 2 + 45);
            sketch.noFill();
        }

        if (bestLcText === "local search 2opt") {
            sketch.stroke("#55DC7B");
            sketch.fill("#55DC7B");
            sketch.textSize(16);
            sketch.textStyle(sketch.NORMAL);
            sketch.text('And Local Search with 2-opt', width - 250, height / 2 + 45);
            sketch.noFill();
        }

        if (bestFirstText !== "" && bestLcText !== "" && document.getElementById('txt_exp') !== null) {
            switch (bestFirstText) {
                case "elastic search":
                    switch (bestLcText) {
                        case "local search swaps":
                            document.getElementById('txt_exp').setAttribute("id", "txt_exp_es_swaps");
                            document.getElementById('link').innerHTML = "<a href='swap_example.html'>swap example</a>";
                            $.MultiLanguage('../../js/language/language.json');
                            break;
                        case "local search 2opt":
                            document.getElementById('txt_exp').setAttribute("id", "txt_exp_es_2opt");
                            document.getElementById('link').innerHTML = "<a href='2opt_example.html'>2-opt example</a>";
                            $.MultiLanguage('../../js/language/language.json');
                            break;
                    }
                    break;
                case "nearest neighbor":
                    switch (bestLcText) {
                        case "local search swaps":
                            document.getElementById('txt_exp').setAttribute("id", "txt_exp_nn_swaps");
                            document.getElementById('link').innerHTML = "<a href='swap_example.html'>swap example</a>";
                            $.MultiLanguage('../../js/language/language.json');
                            break;
                        case "local search 2opt":
                            document.getElementById('txt_exp').setAttribute("id", "txt_exp_nn_2opt");
                            document.getElementById('link').innerHTML = "<a href='2opt_example.html'>2-opt example</a>";
                            $.MultiLanguage('../../js/language/language.json');
                            break;
                    }
                    break;
                case "random":
                    switch (bestLcText) {
                        case "local search swaps":
                            document.getElementById('txt_exp').setAttribute("id", "txt_exp_rnd_swaps");
                            document.getElementById('link').innerHTML = "<a href='swap_example.html'>swap example</a>";
                            $.MultiLanguage('../../js/language/language.json');
                            break;
                        case "local search 2opt":
                            document.getElementById('txt_exp').setAttribute("id", "txt_exp_rnd_2opt");
                            document.getElementById('link').innerHTML = "<a href='2opt_example.html'>2-opt example</a>";
                            $.MultiLanguage('../../js/language/language.json');
                            break;
                    }
                    break;
            }
        }

        // best order drawing

        sketch.stroke("#55DC7B");
        sketch.strokeWeight(4);
        bestOrder.drawPaths(!nnPlayed);
        sketch.stroke(0);
        sketch.strokeWeight(1);

        if (showNames) {
            staticCities.drawNames([], 8, 5);
            bestOrder.drawNames([0], 8, 5);
        }

        if (bestOrder.list.length === staticCities.list.length)
            $("#score_localsearch").text(((Math.abs(recordDistance - solution) / recordDistance) * 100).toFixed(2) + " %");

    };

    play_pause = function() {
        if (!mouseState) {
            sketch.noLoop();
            mouseState = true;
            document.getElementById('btn_pp').className = "fa fa-play";
        } else {
            sketch.loop();
            mouseState = false;
            document.getElementById('btn_pp').className = "fa fa-pause";

        }
    };

    elasticSearch = function() {
        if (cities.list.length > 0) {
            sketch.noFill();
            sketch.stroke(255, 0, 180);
            sketch.strokeWeight(2);
            sketch.ellipse(elasticCenter.vector.x, elasticCenter.vector.y, elasticRadius * 2, elasticRadius * 2);
            sketch.stroke(0);

            for (let i = 0; i < cities.list.length; i++) {
                let d = sketch.dist(elasticCenter.vector.x, elasticCenter.vector.y, cities.list[i].vector.x, cities.list[i].vector.y);
                if (d <= elasticRadius) {
                    bestOrder.addCity(cities.list[i]);
                    cities.removeCity(i);
                }
            }
            elasticRadius+=5;
        }
        bestOrder.calcDistance();
        if (cities.list.length === 0)
            elPlayed = false;
    };

    randomShuffle = function() {
        if (cities.list.length === staticCities.list.length)
            cities.shuffle();
        bestOrder.addCity(cities.list[0]);
        bestOrder.calcDistance();
        cities.removeCity(0);
        if (cities.list.length === 0)
            rndPlayed = false;
    };

    nearestNeighborg = function() {
        if (cities.list.length > 0) {
            let current = bestOrder.list[bestOrder.list.length - 1];
            let c = current.nearestNeighbourg(cities);
            bestOrder.addCity(c);
        }
        bestOrder.calcDistance();
        if (cities.list.length === 0)
            nnPlayed = false;
    };

    localSearchSwaps = function() {
        bestSol = cities.myscore;
        bestI = -1;
        bestJ = -1;
        for (let i = 0; i < cities.list.length; i++) {
            for (let j = i + 1; j < cities.list.length; j++) {
                cities.swapCities(i, j);
                cities.calcDistance();
                if (cities.myscore < bestSol) {
                    bestSol = cities.myscore;
                    bestI = i;
                    bestJ = j;
                }
                cities.swapCities(j, i);
                cities.calcDistance();

            }
        }
        if (bestI !== -1) {
            cities.swapCities(bestI, bestJ);
            cities.calcDistance();
            bestOrder.copy(cities);
        }

        let d = bestOrder.myscore;
        counter++;

        if (counter > maxAttempts) {
            lcswapsPlayed = false;
        }
        if (d < recordDistance) {
            counter = 0;
            recordDistance = d;
        }
    };

    localSearch2opt = function() {
        bestSol = cities.myscore;
        bestI = -1;
        bestJ = -1;

        for (let i = 1; i < cities.list.length - 1; i++) {
            for (let j = i + 1; j < cities.list.length; j++) {
                cities.two_opt(i, j);
                cities.calcDistance();
                if (cities.myscore < bestSol) {
                    bestSol = cities.myscore;
                    bestI = i;
                    bestJ = j;
                }
                cities.two_opt(j, i);
                cities.calcDistance();
            }
        }
        if (bestI !== -1) {
            cities.two_opt(bestI, bestJ);
            cities.calcDistance();
            bestOrder.copy(cities);
        }

        let d = bestOrder.myscore;
        counter++;
        if (counter > maxAttempts)
            lc2optPlayed = false;

        if (d < recordDistance) {
            counter = 0;
            recordDistance = d;
            bestOrder.copy(cities);
        }
    };

    play_NN = function() {
        if (!nnBlock) {
            nnPlayed = true;
            nnBlock = true;
            elBlock = true;
            rndBlock = true;
            document.getElementById('launch_el').className = "btn btn-success btn-outline-light disabled";
            document.getElementById('launch_rnd').className = "btn btn-success btn-outline-light disabled";
        }
    };

    play_EL = function() {
        if (!elBlock) {
            elPlayed = true;
            elBlock = true;
            nnBlock = true;
            rndBlock = true;
            document.getElementById('launch_nn').className = "btn btn-success btn-outline-light disabled";
            document.getElementById('launch_rnd').className = "btn btn-success btn-outline-light disabled";
        }
    };

    play_RND = function() {
        if (!rndBlock) {
            rndPlayed = true;
            rndBlock = true;
            elBlock = true;
            nnBlock = true;
            document.getElementById('launch_nn').className = "btn btn-success btn-outline-light disabled";
            document.getElementById('launch_el').className = "btn btn-success btn-outline-light disabled";
        }
    };

    displayNames = function() {
        showNames = !showNames;
    };

    play_LCSwaps = function() {
        if (!lcswapsBlock) {
            lcswapsPlayed = true;
            lcswapsBlock = true;
            lc2optBlock = true;
            document.getElementById('launch_lc2opt').className = "btn btn-success btn-outline-light disabled";
        }
    }

    play_LC2opt = function() {
        if (!lc2optBlock) {
            lc2optPlayed = true;
            lc2optBlock = true;
            lcswapsBlock = true;
            document.getElementById('launch_lcswaps').className = "btn btn-success btn-outline-light disabled";
        }
    }
};
new p5(sketchLC);