var sketchSwaps = function (sketch) {
    let cities = new Cities(sketch);

    let mouseState = false;

    let randA = 0;
    let randB = 0;

    let width;
    let height;

    let frameCount = 0;

    sketch.setup = function () {
        let divHeight = document.getElementById("tsp_2opt").clientHeight;
        let divWidth = document.getElementById("tsp_2opt").clientWidth;

        let canvas = sketch.createCanvas(divWidth, divHeight - 100);
        canvas.parent('tsp_2opt');
        // canvas.style('z-index', '-1');

        // This is the best order !
        width = canvas.width;
        height = canvas.height;

        cities.addCity(new City(width / 4, height / 4, 8, sketch, "1")); // 0
        cities.addCity(new City(3 * width / 4, 3 * height / 4, 8, sketch, "4")); // 1
        cities.addCity(new City(width / 2, 3 * height / 4 + 50, 8, sketch, "3")); // 2
        cities.addCity(new City(width / 4, 3 * height / 4, 8, sketch, "2")); // 3
        cities.addCity(new City(3 * width / 4, height / 4, 8, sketch, "5")); // 4




    };

    sketch.draw = function () {
        sketch.background(255);
        sketch.stroke(0);
        sketch.strokeWeight(4);
        cities.drawCities();
        cities.drawNames();

        sketch.frameRate(.8);


        if (frameCount % 3 === 0) {
            sketch.stroke("#55DC7B");
            sketch.strokeWeight(3);
            cities.drawPaths(true);

        } else if (frameCount % 3 === 1) {
            sketch.stroke("#55DC7B");
            sketch.strokeWeight(3);
            cities.drawPaths(true);

            sketch.stroke(255, 0, 0);
            sketch.strokeWeight(4);
            sketch.line(cities.list[0].vector.x, cities.list[0].vector.y, cities.list[1].vector.x, cities.list[1].vector.y);
            sketch.line(cities.list[3].vector.x, cities.list[3].vector.y, cities.list[4].vector.x, cities.list[4].vector.y);
        } else if (frameCount % 3 === 2) {
            cities.two_opt(1, 3);
            sketch.stroke("#55DC7B");
            sketch.strokeWeight(3);
            cities.drawPaths(true);

            sketch.stroke(255, 0, 0);
            sketch.strokeWeight(4);
            sketch.line(cities.list[0].vector.x, cities.list[0].vector.y, cities.list[1].vector.x, cities.list[1].vector.y);
            sketch.line(cities.list[3].vector.x, cities.list[3].vector.y, cities.list[4].vector.x, cities.list[4].vector.y);
        }
        frameCount++;
    };

    play_pause = function () {
        if (!mouseState) {
            sketch.noLoop();
            mouseState = true;
            document.getElementById('btn_pp').className = "fa fa-play";
        } else {
            sketch.loop();
            mouseState = false;
            document.getElementById('btn_pp').className = "fa fa-pause";

        }
    };
};
new p5(sketchSwaps);