var sketchSwaps = function (sketch) {
    let cities = new Cities(sketch);

    let mouseState = false;

    let randA = 0;
    let randB = 0;

    let width;
    let height;

    let frameCount = 0;

    sketch.setup = function () {
        let divHeight = document.getElementById("tsp_swaps").clientHeight;
        let divWidth = document.getElementById("tsp_swaps").clientWidth;

        let canvas = sketch.createCanvas(divWidth, divHeight - 100);
        canvas.parent('tsp_swaps');
        // canvas.style('z-index', '-1');

        // This is the best order !
        width = canvas.width;
        height = canvas.height;

        cities.addCity(new City(width / 4, height / 4, 8, sketch, "1")); // 1
        cities.addCity(new City(width / 4, 3 * height / 4, 8, sketch, "2")); // 2
        cities.addCity(new City(3 * width / 4, height / 4, 8, sketch, "3")); // 3
        cities.addCity(new City(3 * width / 4, 3 * height / 4, 8, sketch, "4")); // 4


    };

    sketch.draw = function () {
        sketch.background(255);
        sketch.stroke(0);
        sketch.strokeWeight(4);
        cities.drawCities();

        sketch.frameRate(.8);

        sketch.fill(0);
        sketch.textSize(32);
        sketch.text("order : ", width / 4 + 125, 3 * height / 4 + 75);

        if (frameCount % 3 === 0) {
            randA = Math.floor(Math.random() * 3);
            randB = Math.floor(Math.random() * 3);
            if (randA === randB)
                randB < 3 ? randB = randB + 1 : randB = randB - 1;
            sketch.stroke("#55DC7B");
            sketch.strokeWeight(3);
            cities.drawPaths(true);
            cities.drawNames([randA, randB]);


            for (let i = 0; i < cities.list.length;i++) {
                if (i === randA || i === randB) {
                    sketch.stroke(255,0,0);
                    sketch.fill(255,0,0);
                } else {
                    sketch.stroke(0);
                    sketch.fill(0);
                }
                sketch.text(cities.list[i].name, width / 4 + 125 + 75 + 50 * (i + 1), 3 * height / 4 + 75);
            }
            sketch.noFill();

        }
        else if (frameCount%3 === 1) {
            let temp = new Cities(sketch);
            temp.copy(cities);
            cities.swapCities(randA, randB);

            sketch.stroke("#55DC7B");
            sketch.strokeWeight(3);
            temp.drawPaths(true);
            cities.drawNames([randA, randB]);

            for (let i = 0; i < cities.list.length;i++) {
                if (i === randA || i === randB) {
                    sketch.stroke(255,0,0);
                    sketch.fill(255,0,0);
                } else {
                    sketch.stroke(0);
                    sketch.fill(0);
                }
                sketch.text(cities.list[i].name, width / 4 + 125 + 75 + 50 * (i + 1), 3 * height / 4 + 75);
            }
            sketch.noFill();
        }
        else {
            sketch.stroke("#55DC7B");
            sketch.strokeWeight(3);
            cities.drawPaths(true);
            cities.drawNames([randA, randB]);

            for (let i = 0; i < cities.list.length;i++) {
                if (i === randA || i === randB) {
                    sketch.stroke(255,0,0);
                    sketch.fill(255,0,0);
                } else {
                    sketch.stroke(0);
                    sketch.fill(0);
                }
                sketch.text(cities.list[i].name, width / 4 + 125 + 75 + 50 * (i + 1), 3 * height / 4 + 75);
            }
            sketch.noFill();
        }

        frameCount++;
    };

    play_pause = function() {
        if (!mouseState) {
            sketch.noLoop();
            mouseState = true;
            document.getElementById('btn_pp').className = "fa fa-play";
        } else {
            sketch.loop();
            mouseState = false;
            document.getElementById('btn_pp').className = "fa fa-pause";

        }
    };
};
new p5(sketchSwaps);