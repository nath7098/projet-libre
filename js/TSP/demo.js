let citiesSize = 8;
let width;
let height;

var sketch1 = function(sketch) {
    let cities = new Cities(sketch);
    let bestOrder = new Cities(sketch);
    let totalCities = 8;
    let recordDistance = 0;

    sketch.preload = function() {
        blockImg = sketch.loadImage('../../img/France.jpg');
    };

    sketch.setup = function() {
        let divHeight = document.getElementById("swaps").clientHeight;
        let divWidth = document.getElementById("swaps").clientWidth;

        let swapsCanvas = sketch.createCanvas(divWidth ,divHeight);
        swapsCanvas.parent('swaps');
        swapsCanvas.style('z-index', '-1');

        // This is the best order !
        width = swapsCanvas.width;
        height = swapsCanvas.height;

        cities.addCity(new City(width / 3 - 55, height / 3 - 30, citiesSize, sketch, "Brest")); // Brest
        cities.addCity(new City(width / 3 - 30, height / 3 - 10, citiesSize, sketch, "Lorient")); // Lorient
        cities.addCity(new City(width / 3 + 5, height / 3 + 10, citiesSize, sketch, "Nantes")); // Nantes
        cities.addCity(new City(width / 3 + 22, height / 3 + 55, citiesSize, sketch, "La Rochelle")); // La Rochelle
        cities.addCity(new City(width / 3 + 35, height / 3 + 95, citiesSize, sketch, "Bordeaux")); // Bordeaux
        cities.addCity(new City(width / 3 + 40, height / 2 + 90, citiesSize, sketch, "Pau")); // Pau
        cities.addCity(new City(width / 3 + 80, height / 2 + 85, citiesSize, sketch, "Toulouse")); // Toulouse
        cities.addCity(new City(width / 3 + 125, height / 2 + 115, citiesSize, sketch, "Perpignan")); // Perpignan
        cities.addCity(new City(width / 2 - 30, height / 2 + 85, citiesSize, sketch, "Montpellier")); // Montpellier
        cities.addCity(new City(width / 2 - 20, height / 2 + 75, citiesSize, sketch, "Nimes")); // Nimes
        cities.addCity(new City(width / 2 + 10, height / 2 + 92, citiesSize, sketch,"Marseille")); // Marseille
        cities.addCity(new City(width / 2 + 22, height / 2 + 97, citiesSize, sketch,"Toulon")); // Toulon
        cities.addCity(new City(width / 2 + 60, height / 2 + 75, citiesSize, sketch,"Nice")); // Nice
        cities.addCity(new City(width / 2 + 20, height / 3 + 90, citiesSize, sketch,"Grenoble")); // Grenoble
        cities.addCity(new City(width / 2, height / 2 + 5, citiesSize, sketch,"Lyon")); // Lyon
        cities.addCity(new City(width / 2 - 10, height / 2 + 20, citiesSize, sketch,"St Etienne")); // St Etienne
        cities.addCity(new City(width / 3 + 130, height / 2 + 7, citiesSize, sketch, "Clermont-ferrand")); // Clermont-ferrand
        cities.addCity(new City(width / 3 + 80, height / 2, citiesSize, sketch,"Limoges")); // Limoges
        cities.addCity(new City(width / 3 + 60, height / 3 + 42, citiesSize, sketch, "Poitiers")); // Poitiers
        cities.addCity(new City(width / 3 + 40, height / 3 + 5, citiesSize, sketch, "Angers")); // Angers
        cities.addCity(new City(width / 3 + 55, height / 3 - 15, citiesSize, sketch, "Le Mans")); // Le Mans
        cities.addCity(new City(width / 3 + 67, height / 3 + 7, citiesSize, sketch,"Tours")); // Tours
        cities.addCity(new City(width / 3 + 97, height / 3 - 7, citiesSize, sketch,"Orleans")); // Orleans
        cities.addCity(new City(width / 2 - 27, height / 3 - 22, citiesSize, sketch,"Troyes")); // Troyes
        cities.addCity(new City(width / 2 - 2, height / 3 + 10, citiesSize, sketch,"Dijon")); // Dijon
        cities.addCity(new City(width / 2 + 25, height / 3 + 10, citiesSize, sketch,"Besancon")); // Besancon
        cities.addCity(new City(width / 2 + 57, height / 3 - 37, citiesSize, sketch,"Strasbourg")); // Strasbourg
        cities.addCity(new City(width / 2 + 22, height / 3 - 52, citiesSize, sketch,"Metz")); // Metz
        cities.addCity(new City(width / 2 - 25, height / 3 - 55, citiesSize, sketch,"Reims")); // Reims
        cities.addCity(new City(width / 3 + 110, height / 3 - 40, citiesSize, sketch,"Paris")); // Paris
        cities.addCity(new City(width / 3 + 107, height / 3 + -80, citiesSize, sketch,"Amiens")); // Amiens
        cities.addCity(new City(width / 3 + 125, height / 3 - 107, citiesSize, sketch,"Lille")); // Lille
        cities.addCity(new City(width / 3 + 97, height / 3 - 115, citiesSize, sketch,"Calais")); // Calais
        cities.addCity(new City(width / 3 + 57, height / 3 - 65, citiesSize, sketch,"Le Havre")); // Le Havre
        cities.addCity(new City(width / 3 + 40, height / 3 - 55, citiesSize, sketch,"Caen")); // Caen
        cities.addCity(new City(width / 3 + 10, height / 3 - 20, citiesSize, sketch,"Rennes")); // Rennes

        cities.list = sketch.shuffle(cities.list);

        cities.calcDistance();
        recordDistance =  cities.myscore;
        bestOrder.copy(cities);

        sketch.frameRate(5);

    };

    sketch.draw = function() {
        sketch.frameRate(2);
        sketch.background(255);

        sketch.push();
        sketch.translate(width / 4, 0);
        sketch.image(blockImg, 0, 0, height, height);
        sketch.pop();

        bestOrder.drawCities();

        cities.drawPaths( );

        let preview = new Cities();
        preview.copy(cities);

        cities.shuffle();

        sketch.stroke(100, 0, 100);
        preview.drawPaths(true);
        sketch.stroke(0);

        cities.calcDistance();

    };
};
new p5(sketch1);