let cities = new Cities();
let population = [];
let bestOrder = new Cities();
let totalCities = 10;
let citiesSize = 8;
let popSize = 20;
let recordDistance = 0;
let perent_pop_percentage = 0.3;
let mutationRate = 0.95;

function setup() {
    let divHeight = document.getElementById("tsp_genetic").clientHeight;
    let divWidth = document.getElementById("tsp_genetic").clientWidth;

    let canvas = createCanvas(divWidth ,divHeight - 100);
    canvas.parent('tsp_genetic');
    // canvas.style('z-index', '-1');

    //set up the  first city
    for (let i = 0; i < totalCities; i++) {
        let city = new City(random(citiesSize/2, width-citiesSize/2), random(citiesSize/2, height-citiesSize/2), citiesSize);
        cities.addCity(city);
    }
    cities.calcDistance();

    //set up the population
    population.push(cities);
    for(let i = 0; i < totalCities-1; i++){
        population.push(population[i].shuffle());
    }

    //sort the population
    population = sort_population(population);
    //initialise the display
    recordDistance =  population[0];
    bestOrder.copy(population[0]);
    document.getElementById("score_genetic").innerHTML = recordDistance;


}

function draw() {
    //set up the kid population
    let kids_population = [];
    for(let b = 0; b < 2*population.length; b++) {
        kids_population.push(kid_gen());
    }
    //new population selection:
    //sort the kid population

    kids_population = sort_population(kids_population);
    //part of the best parent population saved
    let newPopulation = population.splice(popSize*perent_pop_percentage,popSize-popSize*perent_pop_percentage);
    //part of the best kids population saved.
    for(let index = popSize*perent_pop_percentage; index < popSize; index ++){
        newPopulation.push(kids_population.shift());
    }
    population = newPopulation;
    bestOrder.copy(newPopulation[0]);


    /*background(255);

    bestOrder.drawCities();

    cities.drawPaths();

    stroke("#55DC7B");
    strokeWeight(4);
    bestOrder.drawPaths();
    stroke(0);
    strokeWeight(1);

    cities.calcDistance();
    let d = cities.myscore;
    if (d < recordDistance) {
        recordDistance = d;
        bestOrder.copy(cities);
        document.getElementById("score_localsearch").innerHTML = recordDistance;
    }*/
}

function kid_gen() {
    //parent selection : we take 2 random parents and we identify the best(parent1)
    let kid = new Cities();
    let parent1 = new Cities();
    let parent2 = new Cities();
    let i = Math.floor(Math.random() * (cities.list.length));
    let j = Math.floor(Math.random() * (cities.list.length));
    if(i<j){
        parent1.copy(population[i]);
        parent2.copy(population[j]);
    }
    else{
        parent1.copy(population[j]);
        parent2.copy(population[i]);
    }
    //kid formation :
    //we construct the kid with the first half of the best parent genes sequence then we fill the second half with
    //the genes of the second parent taken 1 by 1 ordered by their position in the second parent genes sequence.
    let citiesParent1 = parent1.list.splice(popSize/2-1,popSize/2);
    let citiesParent2 = parent2.list;
    for(let i=0;i<popSize;i++){
        //Verify the element citiesParent2[i] isn't present in the array citiesParent1
        let posCity = citiesParent1.indexOf(citiesParent2[i]);
        if(posCity === -1 && !(typeof citiesParent2[i]==='undefined')){
            //if the element isn't in citiesParent we add it to citiesParent1
            citiesParent1.push(citiesParent2[i]);
        }
    }
    kid.list = citiesParent1.slice();

    kid.calcDistance();
    let mutationChance = Math.random();
    //Mutation :
    //the mutation consiste of the exchange of the position of 2 genes in the genes sequence
    //it happens only if the mutation chance is upper than the the mutationRate

    if(mutationChance > mutationRate){
        let x = Math.floor(Math.random() * (cities.list.length));
        let y = Math.floor(Math.random() * (cities.list.length));
        kid.swapCities(x,y);
    }
    return kid;
}

function sort_population(population){
    let min_index;
    for(let i = 0; i < population.length; i++){
        min_index = i;
        for (let j = i+1; j<population.length; j++){
            if(population[min_index].myscore > population[j].myscore){
                min_index = j;
            }
        }
        let temp = population[i];
        population[i] = population[min_index];
        population[min_index] = temp;
    }

    return population;
}




