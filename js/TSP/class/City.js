class City {

    constructor(x, y, citySize, sketch, name= "") {
        this.vector = sketch.createVector(x, y);
        this.size = citySize;
        this.sketch = sketch;
        this.name = name;
    };

    /**
     * Method to draw a circle representing a city
     * @param b if true, the ellipse is twice as big as normal (for the first city), else nothing
     */
    drawEllipse = function (b) {
        this.sketch.ellipse(this.vector.x, this.vector.y, b * this.size + this.size, b * this.size + this.size);
    };

    /**
     * Method to the current city's nearest neighbor in a Cities object and removes it from the list
     * @param cities the Cities object containing the list of all cities
     * @returns {null|*|number|[]|string} a copy of the nearest city or null if there is no city
     */
    nearestNeighbourg = function (cities) {
        let nearestCity;
        let nearestIndex = 0;
        let nearestDistance = 99999;
        if (cities.list !== null && cities.list.length > 0) {
            for (let i = 0; i < cities.list.length ; i++) {
                let d = this.sketch.dist(cities.list[i].vector.x, cities.list[i].vector.y, this.vector.x, this.vector.y);
                if (d < nearestDistance && d > 0) {
                    nearestCity = cities.list[i];
                    nearestIndex = i;
                    nearestDistance = d;
                }
            }
            cities.removeCity(nearestIndex);
            return nearestCity;
        }
        return null;
    };

    /**
     * Displays the name of the city
     * @returns {string} the name of the city
     */
    toString() {
        return this.name;
    }


}