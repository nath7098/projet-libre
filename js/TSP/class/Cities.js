class Cities {
    constructor(sketch) {
        this.list = [];
        this.myscore = 0.0;
        this.sketch = sketch;
    }

    /**
     * Method to create a copy of Cities Object
     * @param toCopy The Cities Object to copy
     */
    copy = function (toCopy) {
        this.list = toCopy.list.slice();
        this.myscore = toCopy.myscore;
        this.sketch = toCopy.sketch;
    };

    /**
     * Method to append a city at the end of the list
     * @param city to be added
     */
    addCity = function (city) {
        this.list.push(city);
    };

    /**
     * Method to add a city at a given index
     * @param city to be added
     * @param index where to add
     */
    addCityAtIndex = function(city, index) {
        this.list.splice(index, 0, city);
    };

    /**
     * Method to remove a city at a given index
     * @param index of the city to be removed
     */
    removeCity = function (index) {
        this.list.splice(index, 1);
    };

    /**
     * Method to empty the whole cities list
     */
    emptyCities = function() {
        this.list.splice(0, this.list.length);
    };

    /**
     * Method to swap two cities
     * @param i index of the first city
     * @param j index of the second city
     */
    swapCities = function (i, j) {
        if (this.list !== []) {
            let tmp = this.list[i];
            this.list[i] = this.list[j];
            this.list[j] = tmp;
        }
    };

    /**
     * Method to do the swaps in the 2opt algorithm
     * @param i index of the first city
     * @param j index of the last city
     */
    swapOpt = function (i, j) {
        this.swapCities(i + 1, j);
    };

    /**
     * Alternative method to apply 2opt algorithm
     * @param i index of the first city
     * @param j index of the last city
     */
    twoOptSwap = function(i, j) {
        let newTour = new Cities(this.sketch);
        for (let k = 0; k <= i - 1; k++) {
            newTour.addCity(this.list[k]);
        }
        let dec = 0;
        for (let k = i; k <= j; k++) {
            newTour.addCity(this.list[j - dec]);
            dec++;
        }
        for (let k = j + 1; k < this.list.length; k++) {
            newTour.addCity(this.list[k]);
        }
        newTour.calcDistance();
        this.copy(newTour);
    };

    /**
     * Method to calculate the length of the path through
     * every cities back to the first one
     * @returns {number} the score (total distance)
     */
    calcDistance = function () {
        let sum = 0;
        for (let i = 0; i < this.list.length - 1; i++) {
            let d = this.sketch.dist(this.list[i].vector.x, this.list[i].vector.y, this.list[i + 1].vector.x, this.list[i + 1].vector.y);
            sum += d;
        }
        sum += this.sketch.dist(this.list[this.list.length - 1].vector.x, this.list[this.list.length - 1].vector.y, this.list[0].vector.x, this.list[0].vector.y);
        this.myscore = sum;
        return this.myscore;
    };

    /**
     * Method to draw the cities
     * @param bool if true, the first city is colored in red, else it is black
     */
    drawCities = function (bool = true) {
        this.sketch.fill(0);
        let i = 0;
        let sk = this;

        this.list.forEach(function (c) {
            if (i === 0 && bool) {
                sk.sketch.fill(255, 0, 0);
                sk.sketch.stroke(255, 0, 0);
                c.drawEllipse(true);

            } else {
                sk.sketch.fill(0);
                sk.sketch.stroke(0);
                c.drawEllipse(false);

            }
            i++;
        });
        this.sketch.stroke(0);
        this.sketch.strokeWeight(1);
    };

    /**
     * Method to draw the path between the cities
     * @param bool if true, the last and the first cities in the list are connected, else they are not
     */
    drawPaths = function (bool) {
        this.sketch.beginShape();
        this.sketch.noFill();
        let sk = this;
        this.list.forEach(function (c) {
            sk.sketch.vertex(c.vector.x, c.vector.y);
        });
        if (bool)
            this.sketch.vertex(this.list[0].vector.x, this.list[0].vector.y);
        this.sketch.endShape();
    };

    /**
     * Method to draw cities' names next to them
     * @param indexesList The list of the indexes we want to highlight in red
     * @param size the size of the text
     * @param distanceFromPoint the distance of position from the point
     */
    drawNames = function(indexesList = [], size = 24, distanceFromPoint = 15) {
        let that = this;
        let i = 0;
        this.list.forEach(function (c) {
            that.sketch.strokeWeight(0);
            that.sketch.textSize(size);
            that.sketch.textStyle(that.sketch.BOLD);
            that.sketch.stroke(0);
            if (indexesList.length === 0)
                that.sketch.fill(0);
            else {
                if (indexesList.includes(i)) {
                    that.sketch.stroke(255,0,0);
                    that.sketch.fill(255,0,0);
                } else {
                    that.sketch.stroke(0);
                    that.sketch.fill(0);
                }

            }
            that.sketch.text(c.name,c.vector.x + distanceFromPoint, c.vector.y - distanceFromPoint);
            that.sketch.noFill();
            that.sketch.stroke(0);
            i++;
        });
        this.sketch.strokeWeight(2);
    };

    /**
     * Method to check if we can aplly the 2opt algorithm and to apply it if possible
     * @param i first city
     * @param j last city
     */
    two_opt = function (i, j) {
        if (j !== i - 1 && j !== i && j !== i + 1) {
            if (i > j) {
                let tmp = j;
                j = i;
                i = tmp;
            }
            this.twoOptSwap(i, j);
        }
    };

    /**
     * Method to randomly shuffle the list of cities
     */
    shuffle = function (){
        this.list =  this.sketch.shuffle(this.list);
    };

}