function expand() {
    var btn = document.getElementById('btn_expand');
    if (btn.className === "fa fa-chevron-up") {
        btn.className = "fa fa-chevron-down";
    }
    else
        btn.className = "fa fa-chevron-up";

    $('#body_expand').collapse('toggle');
}